#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "stack.h"
#include "intopost.h"
#include "exp.h"

#define POSTFIX_BUFFER 50
#define INFIX_BUFFER 60

int main()
{ 
  char infix[INFIX_BUFFER] = {0};
  char postfix[POSTFIX_BUFFER] = {0};
  printf("Enter infix Expression...\n");
  scanf("%s", infix);
  infix_to_postfix(infix, postfix, POSTFIX_BUFFER);
  printf("The Postfix Expression : %s\n",postfix);
  postevaluvation(postfix);
}

