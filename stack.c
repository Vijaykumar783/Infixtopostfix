#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "stack.h"


void stack_init(stack *s)
{
  s->data[0] = '\0';
  s->top = 0;
}

int stack_empty(stack *s)
{
  if(s->top == 0)
    return(0);
  return(1);
}

int stack_full(stack *s)
{
  if(s->top == STACK_LEN - 1)
    return(1);
  return(0);
}

int  stack_push(stack *s,int x)
{
  s->top++;
  s->data[s->top] = x;
  return (0);
}

int stack_pop(stack *s)
{
     return s->data[s->top--];
  
}

int stack_top(stack *p)
{
  return (p->data[p->top]);
}


