#include <string.h>
#include <stdio.h>
#include "stack.h"
#include "exp.h"


void parse_ifx(char *ifx, int *pfx)
{
  int i;
  int j = 0;
  for (i = 0; ifx[i]; i++) {
    if (isalnum(ifx[i])) {
      pfx[j] = pfx[j] * 10 + (ifx[i] - 48);
    } else if (ifx[i] == ' ') {
      
      j++;
    } else {
      pfx[j] = ifx[i];
    }
  }
}

void postevaluvation(char postfix[])
{
  int ch;
  int k = 0;
  int op1;
  int op2;
  int pfx[50] = {0};
  
  stack stack2;
  stack_init(&stack2);
  parse_ifx(postfix, pfx);

  while ((ch = pfx[k])) {
    if ((ch == '+') || (ch == '-') || (ch == '*') || (ch == '/')) {
      op2 = stack_pop(&stack2);
      op1 = stack_pop(&stack2);
      switch (ch) {
      case '+':
	stack_push(&stack2, op1+op2); break;
      case '-':
	stack_push(&stack2, op1-op2); break;
      case '*':
	stack_push(&stack2, op1*op2); break;
      case '/':
	stack_push(&stack2, op1/op2); break;
      }
    } else {
      stack_push(&stack2, pfx[k]);
    }
    k++;
  }	
  printf(" postfix Evaluation Result: %d\n",stack_top(&stack2));
}



