#ifndef intopost_h
#define intopost_h
//#include <stdlib.h>

int precedence(char x);
void infix_postfix(char *infix, char *postfix, size_t buffer);

#endif
