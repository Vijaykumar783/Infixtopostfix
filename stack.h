#ifndef stack_h
#define stack_h

#define STACK_LEN 50


typedef struct stack
{
  int data [STACK_LEN];
  int top;
}stack;

void stack_init(stack *s);
int stack_empty(stack *s);
int stack_full(stack *s);
int stack_push(stack *s, int x);
int stack_pop(stack *s);
int stack_top(stack *p);

#endif 
