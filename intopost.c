#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "stack.h"
#include "intopost.h"

int precedence(char x)
{
  if (x=='+'||x=='-')
    return(1);
  if (x=='*'||x=='/'||x=='%')
    return(2);
  return(3);
}

void prioprity(char infix[], char postfix[])
{
  int i;
  int j=0;
  int ret;
  int tp;
  int pr1;
  int pr2;
  int tmp;
  
  stack stack;
  stack_init(&stack);
  
  pr1 = precedence(tp);
  pr2 = precedence(infix[i]);
  while (tp) {
    if (precedence(tp) > precedence(infix[i])) {
      tmp = stack_pop(&stack);
      postfix[j++] = ' ';
      postfix[j++] = tmp;
      postfix[j++] = ' ';
      
      tp = stack_top(&stack);
      if (!tp)
	stack_push(&stack, infix[i]);
    } else if (precedence(tp) == precedence(infix[i])) {
      tmp = stack_pop(&stack);
      postfix[j++] = ' ';
      postfix[j++] = tmp;
      postfix[j++] = ' ';
           
      stack_push(&stack, infix[i]);
      break;
    } else {
      postfix[j++] = ' ';
      stack_push(&stack, infix[i]);          
      break;
    }
  }			

}

void  infix_to_postfix(char *infix, char *postfix, size_t buffer)
{
  int i;
  int j=0;
  int ret;
  int tp;
  int tmp;
  int opt1;
  int opt2;  
  char ifx;
  int len;

  stack stack;
  stack_init(&stack);

  len = strlen(infix);
  ifx = infix[len - 1];
  if ((ifx == '+') || ((ifx == '-')) || (ifx == '*') || (ifx == '/')) {
    printf(" invalid infix expression!\n");
    exit(0);
  }
  
  for (i = 0; infix[i] && j < buffer; i++) {
    if (isalnum(infix[i])) {
      postfix[j++] = infix[i];
      
    } else if (infix[i] == '(') {
      stack_push(&stack, infix[i]);
    } else if (infix[i] == ')') {
      tp = stack_top(&stack);

      while (tp != '(') {
	tmp = stack_pop(&stack);
	postfix[j++] = ' ';
	postfix[j++] = tmp;
	tp = stack_top(&stack);
      }
      stack_pop(&stack);

    } else {
      ret = stack_empty(&stack);

      if (!ret) {
	postfix[j++] = ' ';
	stack_push(&stack, infix[i]);
	continue;
      } else {
	tp = stack_top(&stack);
	if (tp == '(') {
	  stack_push(&stack, infix[i]);
	  postfix[j++] = ' ';
	  continue;
	}
	prioprity(infix, postfix);
      }
    }
  }

  while (stack_empty(&stack)) {
    tmp = stack_pop(&stack);
    postfix[j++] = ' ';
    postfix[j++] = tmp;    
  } 
  postfix[j] = '\0';
}




